package com.livro.capitulo3.crudjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

public class ContatoCrudJDBC {

	public void salvar(Contato contato){
		//Connection conexao = this.geraConexao();
		PreparedStatement insereSt = null;
		
		String sql = "insert into contato(nome, telefone, email, dt-cad, obs) values(?,?,?,?,?)";
		try {
			//insereSt = conexao.prepareStatement(sql);
			insereSt.setString(1, contato.getNome());
			insereSt.setString(1, contato.getTelefone());
			insereSt.setString(1, contato.getEmail());
			insereSt.setDate(1, contato.getDataCadastro());
			insereSt.setString(1, contato.getObservacao());
			insereSt.executeUpdate();			
		} catch (Exception e) {
			System.out.println("Erro ao incluir contato. mensagem: " + e.getMessage());
		}finally{
			try {
				insereSt.close();
				//conexao.close();
			} catch (Throwable e) {
				System.out.println("Erro ao fechar opera��es de inser��o. mensagem: " + e.getMessage());
			}
		}
	}
	public void atualizar(Contato contato){}
	public void excluir(Contato contato){}
	public List<Contato> listar(){
		//Connection conexao = this.geraConexao();
		List<Contato> contatos = new ArrayList<Contato>();
		Statement consulta = null;
		ResultSet resultado = null;
		Contato contato = null;
		
		String sql = "select * from contato";
		try {
			//consulta = conexao.createStatement();
			resultado = consulta.executeQuery(sql);
			
			while(resultado.next()){
				contato = new Contato();
				contato.setCodigo(new Integer(resultado.getInt("codigo")));
				contato.setNome(resultado.getString("nome"));
				contato.setTelefone(resultado.getString("telefone"));
				contato.setEmail(resultado.getString("email"));
				contato.setDataCadastro(resultado.getDate("dt_cad"));
				contato.setObservacao(resultado.getString("obs"));
				contatos.add(contato);
			}
		} catch (Exception e) {
			System.out.println("Erro ao buscar c�digo do contato. mensagem: " + e.getMessage());
		}finally{
			try {
				consulta.close();
				resultado.close();
				//conexao.close();
			} catch (Throwable e) {
				System.out.println("Erro ao fechar opera��es de consulta. Mensagem: " + e.getMessage());
			}
		}
		return contatos;
	}	
	//public Contato buscaContato(int valor){}
	//public Connection geraConexao(){}
	public static void main(String[] args){
		ContatoCrudJDBC contatoCRUDJDBC = new ContatoCrudJDBC();
		
		Contato beltrano = new Contato();
		beltrano.setNome("Beltrano Solar");
		beltrano.setTelefone("(47)555-5555");
		beltrano.setEmail("beltrano@email.com.br");
		beltrano.setDataCadastro(new Date(System.currentTimeMillis()));
		beltrano.setObservacao("Novo Cliente");
		contatoCRUDJDBC.salvar(beltrano);
	}
}
